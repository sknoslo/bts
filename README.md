#LD29

Beneath the Surface.

---

#Kolony

A game about an ant colony... wait for it... beneath the surface of the Earth!!!

---

#Ideas

* Evolution
    * start with naive ants, that just bounce around hoping they run into food
    * give random variability to ant speed, based on parent speed (ie fast ants give birth to fast ants, with some variability)
    * give reproduction chances some variablility as well
    * mutation that allows ants to leave scent trails?
    * mutation that allows ants to smell food?
    * etc...

* Weather
    * kills some portion of ants?
    * closes surface openings?

* Death
    * lack of food
    * lack of space

* Reproduction
    * an ant that is 80% full of food is eligible to reproduce
    * 1/10000 chance of reproduction each tick
