GameState = require "hump.gamestate"

require "states.game"
require "states.menu"

function love.load()
    -- love.graphics.setDefaultFilter("nearest", "nearest")

    GameState.registerEvents()
    GameState.switch(menu)
end
