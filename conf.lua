function love.conf(t)
    t.modules.joystick = false
    t.modules.physics = false

    t.console = false

    t.window.title = "Kolony"
    t.window.resizable = false
end
