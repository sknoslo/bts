Class = require "hump.class"

vector = require "hump.vector"

Ant = Class{}

function Ant:init(pathmap, hf, v, sd, rf, sf)
    self.currentNode = pathmap:getRandomNode()
    self.pos = vector(self.currentNode:getX(), self.currentNode:getY())

    self.pathmap = pathmap

    self.hunger = love.math.random(80, 100) -- 100 being completely full, 0 being dead

    -- chance out of 1000 hunger will go down every tick
    hf = hf and hf or love.math.random(15, 25)
    hf = hf + love.math.random(3) - 2 -- hungerfactor of +- 1 of the parent
    self.hungerFactor = hf > 2 and hf or 5 -- minimum of 5

    v = v and v or love.math.random(25, 35)
    v = v + love.math.random(5) - 3 -- velocity +- 2 of parent
    self.velocity = v > 1 and v or 1

    -- the distance this ant can travel on the surface before returning home
    sd = sd and sd or love.math.random(40, 50)
    sd = sd + love.math.random(5) - 3 -- sd +- 2 of parent
    self.surfaceDist = sd > 1 and sd or 1

    -- the chance out of 5000 + pop * 100 that an ant will repopulate each tick
    rf = rf and rf or love.math.random(35)
    rf = rf + love.math.random(3) - 2 -- repopfactor +- 1/2 of parent
    self.repopFactor = rf > 1 and rf or 1

    -- the chance out of 5000 that an ant will find food each tick while on the surface
    sf = sf and sf or love.math.random(10)
    sf = sf + love.math.random(2) - 1 -- +- 1/0 of parent
    self.scavengeFactor = sf > 1 and sf or 1

    self.nextNode = self.currentNode:getRandomNext()
    self.destination = vector(self.nextNode:getX(), self.nextNode:getY())
    self.onsurface = false
end

function Ant:update(dt, killfactor, population)
    local dir = self.destination - self.pos
    if dir:len() < 2 then
        self.pos = self.destination
        -- self.currentNode = self.nextNode
        self:setNewDest()
    else
        dir = dir:normalized()
        self.pos = self.pos + dir * self.velocity * dt
    end

    -- TODO: Actually implement random food drops
    -- each tick there is a sf/pop * 10 chance of finding food if ant is on surface
    if self.onsurface and love.math.random(population * 100) <= self.scavengeFactor then
        self.hunger = 100
    end

    -- randomly repopulate
    if self.hunger > 80 then -- need to be relatively fed, giving birth will take food
        if love.math.random(5000 + population * 100) <= self.repopFactor then
            Signals.emit('reproduce', self)
            self.hunger = self.hunger - love.math.random(5)
        end
    end

    -- randomly decrement hunger
    if love.math.random(1000) <= self.hungerFactor then
        self.hunger = self.hunger - love.math.random(3)
        if self.hunger <= 0 then
            Signals.emit('die', self)
        end
    end

    -- deal with overpopulation
    if killfactor > 0 then
        if love.math.random(population) <= killfactor * love.math.random(20) then
            Signals.emit('die', self)
        end
    end
end

function Ant:draw()
    local r = self.velocity + self.surfaceDist + self.scavengeFactor + self.repopFactor
    r = r > 255 and 255 or r
    love.graphics.setColor(r, 0, 0)
    love.graphics.point(self.pos.x, self.pos.y)
end

function Ant:setNewDest()
    -- TODO: Refactor this, so it actually makes sense...
    if self.nextNode.surfacenode and not self.onsurface then
        self.onsurface = true
        -- dont change the next node, because the ant should return to it
        local dir = love.math.random(2) == 1 and -self.surfaceDist or self.surfaceDist
        self.destination = vector(self.nextNode:getX() + dir, self.pathmap.surface)
        self.currentNode = nil
    elseif self.onsurface and not self.currentNode then
        -- return to surface node
        self.destination = vector(self.nextNode:getX(), self.nextNode:getY())
        self.currentNode = self.nextNode
    else
        self.onsurface = false
        self.currentNode = self.nextNode
        self.nextNode = self.currentNode:getRandomNext()
        self.destination = vector(self.nextNode:getX(), self.nextNode:getY())
    end
end
