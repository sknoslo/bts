Class = require "hump.class"

vector = require "hump.vector"

Node = Class{}

function Node:init(parent, x, y, surfacenode)
    self.parent = parent
    if self.parent then
        self.parent:addChild(self)
    end
    self.pos = vector(x, y)

    self.surfacenode = surfacenode and true or false

    self.children = {}
end

function Node:addChild(child)
    self.children[#self.children+1] = child
end

function Node:getRandomNext()
    local poolsize = #self.children
    if self.parent then poolsize = poolsize + 1 end

    local rand = love.math.random(poolsize)
    if rand > #self.children then
        return self.parent
    else
        return self.children[rand]
    end
end

function Node:getX()
    return self.pos.x
end

function Node:getY()
    return self.pos.y
end



PathMap = Class{}

CLICK_RADIUS = 20

function PathMap:init()
    self.surface = 50 -- y level of the surface

    self.path = {}

    local mid = love.graphics.getWidth() / 2

    self.path[1] = Node(nil, mid, 50, true)

    self.path[2] = Node(self.path[1], mid, 100)
    self.path[3] = Node(self.path[2], mid - 50, 100)
    self.path[4] = Node(self.path[2], mid + 50, 100)

    self.maxPopFactor = .25 -- ie for a pathlength of 100 the population can be 25 safely

    self.pathLength = 150
end

function PathMap:getRoot()
    return self.path[1]
end

function PathMap:getRandomNode()
    return self.path[love.math.random(#self.path)]
end

function PathMap:getMaxPopulation()
    return math.ceil(self.pathLength * self.maxPopFactor)
end

function PathMap:update(dt)
    self.mousepos = vector(love.mouse.getX(), love.mouse.getY());

    if not self.selectedNode then
        for _, node in pairs(self.path) do
            if node.surfacenode then -- dont allow adding children to surfacenode
            elseif (node.pos - self.mousepos):len() < CLICK_RADIUS then
                if self.mousedown then
                    self.selectedNode = node
                    self.mousedown = false
                end
            end
        end
    else
        if self.mousepos.y < self.surface + CLICK_RADIUS then
            self.mousepos.y = self.surface
        end
        if love.mouse.isDown("r") then
            -- if player clicks right mouse, cancel node drawing
            self.selectedNode = nil
            self.mousedown = false
        elseif self.mousedown then
            if self.mousepos.y == self.surface then
                self.path[#self.path+1] = Node(self.selectedNode, self.mousepos.x, self.mousepos.y, true)
            else
                self.path[#self.path+1] = Node(self.selectedNode, self.mousepos.x, self.mousepos.y)
            end

            self.pathLength = self.pathLength + (self.selectedNode.pos - self.mousepos):len()

            self.selectedNode = nil
            self.mousedown = false
        end
    end
end

function PathMap:draw()
    -- draw the earth
    love.graphics.setColor(160, 82, 45)
    love.graphics.rectangle('fill', 0, self.surface, love.graphics.getWidth(), love.graphics.getHeight())

    love.graphics.setColor(139, 69, 19)
    -- outline earth
    love.graphics.rectangle('line', 0, self.surface, love.graphics.getWidth(), love.graphics.getHeight())

    -- draw the paths
    for _, node in pairs(self.path) do
        -- draw a circle when hovering over a node
        if (node.pos - self.mousepos):len() < CLICK_RADIUS
            and not self.selectedNode
            and not node.surfacenode then
            love.graphics.setColor(255, 255, 255)
            love.graphics.circle("line", node:getX(), node:getY(), CLICK_RADIUS, 20)
        end

        -- no loops, so just draw a line from each parent to each of their children
        for _, child in pairs(node.children) do
            love.graphics.setColor(139, 69, 19)
            love.graphics.line(node:getX(), node:getY(), child:getX(), child:getY())
        end
    end

    if self.selectedNode then
        if self.mousepos.y == self.surface then
            love.graphics.setColor(255, 255, 255)
            love.graphics.circle("line", self.mousepos.x, self.surface, CLICK_RADIUS, 20)
        end
    end

    -- draw a line from a selected node to the mouse pointer
    if self.selectedNode then
        love.graphics.setColor(255, 255, 255)
        love.graphics.line(self.selectedNode:getX(), self.selectedNode:getY(), self.mousepos.x, self.mousepos.y)
    end
end

function PathMap:mousepressed()
    self.mousedown = true
end

function PathMap:mousereleased()
    self.mousedown = false
end
