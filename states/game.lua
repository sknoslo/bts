GameState = require "hump.gamestate"

Signals = require "hump.signal"


require "entities.ant"
require "entities.pathmap"

game = {}

function game:enter()
    love.graphics.setFont(love.graphics.newFont(12))
    debug = false

    love.graphics.setBackgroundColor(135, 206, 250)
    pathmap = PathMap()

    local startingants = love.math.random(14, 38) -- start with 4 to 20 ants

    self.antRecord = startingants -- best number of ants

    ants = {}
    for i = 1, startingants do
        ants[#ants+1] = Ant(pathmap)
    end
end

function game:update(dt)
    local maxPop = pathmap:getMaxPopulation()
    local killfactor = #ants - maxPop
    killfactor = killfactor > 0 and killfactor or 0

    for idx, ant in pairs(ants) do
        ant:update(dt, killfactor, maxPop)
        self.antRecord = self.antRecord > #ants and self.antRecord or #ants
    end

    pathmap:update(dt)
end

function game:draw()
    pathmap:draw()

    for key, value in pairs(ants) do
        value:draw()
    end

    love.graphics.setColor(255, 255, 255)
    love.graphics.print("Ants: " .. #ants, 5, 5)
    love.graphics.print("Max Population: " .. pathmap:getMaxPopulation(), 5, 20)
    love.graphics.print("Best: " .. self.antRecord, 5, 35)

    if debug then
        love.graphics.print("FPS: " .. love.timer.getFPS(), 5, love.graphics.getHeight() - 17)
    end
end

function game:keypressed(key)
    if key == "d" then
        debug = not debug
    elseif key == "escape" then
        GameState.switch(menu)
    end
end

function game:mousepressed(x, y, button)
    if button == "l" then pathmap:mousepressed() end
end

function game:mousereleased(x, y, button)
    if button == "l" then pathmap:mousereleased() end
end

Signals.register('reproduce', function(parent)
    ants[#ants+1] = Ant(pathmap, parent.hungerFactor,
        parent.velocity, parent.surfaceDist,
        parent.repopFactor, parent.scavengeFactor)
end)

Signals.register('die', function(ant)
    for k, v in pairs(ants) do
        if v == ant then
            table.remove(ants, k)
        end
    end
end)
