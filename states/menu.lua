GameState = require "hump.gamestate"

menu = {}

function menu:enter()
    love.graphics.setFont(love.graphics.newFont(24))
    self.textalpha = 248
    self.alphaincrement = 2
    self.background = love.graphics.newImage("res/menu_bg.png")
end

function menu:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(self.background, 0, 0)
    self.textalpha = self.textalpha + self.alphaincrement
    if self.textalpha == 250 then
        self.alphaincrement = -2
    elseif self.textalpha == 80 then
        self.alphaincrement = 2
    end
    love.graphics.setColor(255, 255, 255, self.textalpha)
    love.graphics.print("CLICK ANYWHERE TO START", 235, 550)
end

function menu:mousepressed(x, y, button)
    if button == "l" then GameState.switch(game) end
end
